﻿#include <iomanip>
#include <iostream>
#include "file_reader.h"
#include "constants.h"
#include "strakts.h"
#include "filter.h"
#include "sort.h"
using namespace std;

void output(osadki* osad) {
	cout << "Тип...........: ";
	cout << osad->title << " " << endl;;
	cout << "Дата..........: ";
	cout << osad->date.day << '.';
	cout << osad->date.month;
	cout << endl;
	cout << "Количество....: ";
	cout << osad->kolvo;
	cout << '\n';
	cout << '\n';
}

int main() {
	setlocale(0, "");
    cout << "Laboratory work #8. GIT\n";
    cout << "Variant #3 Осадки\n";
    cout << "Author: Кириллов Владислав\n";
    osadki* osad[MAX_FILE_ROWS_COUNT];
    int size;
	read("text.txt", osad, size);
	osadki* osad1 = new osadki[size];
	int set;
	int set1;
	cout << "тип сортировки >> "; cin >> set;
	switch (set){
	case 1: 
		cout << "ввведите параметр сортировки >> "; cin >> set1;
		switch (set1){
		case 1:
			//quick(osad1, size, comp_kolvo);
			/*for (int i = 0; i < size; i++) {
				output2(&osad[i]);
			}*/
			break;
		case 2:

			break;
		default: cout << "неверный параметр\n";
			break;
		}
		break;
	case 2:
		cout << "ввведите параметр сортировки >> "; cin >> set1;
		switch (set1) {
		case 1:

			break;
		case 2:

			break;
		default: cout << "неверный параметр\n";
			break;
		}
		break;
	default: cout << "неверный параметр\n";
		break;
	}

    for (int i = 0; i < size; i++){
        output(osad[i]);
    }


	int seach;
	cout << "введите месяц (1-12) << "; cin >> seach;
	if (seach == 3 || seach == 4 || seach == 8) {
		cout << "итог >> " << sum(osad, size, seach) << endl;
	}
	else {
		cout << "данных об этом месяце нет \n";
	}

	bool (*check_function)(osadki*) = NULL;
	cout << "\nВыберите способ фильтрации или обработки данных:\n";
	cout << "1) Дни когда шел дождь\n";
	cout << "2) Дни когда кол-во осадков было меньше 15\n";
	cout << "\nВведите номер выбранного пункта: ";
	int item;
	cin >> item;
	cout << '\n';
	switch (item)
	{
	case 1:
		check_function = check_name_dogd; // присваиваем в указатель на функцию соответствующую функцию
		cout << "****** дни когда жел дождь *******\n\n";
		break;
	case 2:
		check_function = check_kolvo; // присваиваем в указатель на функцию соответствующую функцию
		cout << "***** количество осадков *****\n\n";
		break;
	default:
		throw "Некорректный номер пункта";
	}
	if (check_function)
	{
		int new_size;
		osadki** filtered = filter(osad, size, check_function, new_size);
		for (int i = 0; i < new_size; i++)
		{
			output(filtered[i]);
		}
		delete[] filtered;
	}



	for (int i = 0; i < size; i++)
	{
		delete osad[i];
	}
	return 0;
}