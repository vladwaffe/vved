#ifndef FILE_READER_H
#define FILE_READER_H

#include "strakts.h"

void read(const char* file_name, osadki* array[], int& size);

#endif