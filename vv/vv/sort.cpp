#include <iostream>
#include "file_reader.h"
#include "constants.h"
#include "strakts.h"
#include "filter.h"
#include "vv.h"
#include "sort.h"
using namespace std;

int sum1(osadki* osad, int size) {
	int rez = 0;
	for (int i = 0; i < size; i++) {
		rez += osad->kolvo;
	}
	return rez;
}

int sum(osadki* osad[], int size, int n) {
	int new_size = 0;
	int k = 0;
	osadki* result = new osadki [size];
	new_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(*osad[i], n))
		{
			result[new_size++] = *osad[i];
			k++;
		}
	}
	int sum = 0;
	sum = sum1(result, k);
	return sum;
}

bool check(osadki a, int n) {
	return a.date.month == n;
}