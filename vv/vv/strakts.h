#ifndef strakts
#define strakts

#include "constants.h"

struct date{
    int day;
    int month;
};

struct osadki{
    date date;
    int kolvo;
    char title[MAX_STRING_SIZE];
};

#endif