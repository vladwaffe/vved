#ifndef FILTER_H
#define FILTER_H

#include "strakts.h"

bool check_name_dogd(osadki* element);

bool check_kolvo(osadki* element);

osadki** filter(osadki* array[], int size, bool (*check)(osadki* element), int& result_size);
//osadki** replase(osadki* array[], int size, void(*check)(osadki* element));

//void qsortkolvo(osadki** osad, int size);



#endif
