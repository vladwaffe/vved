#include "filter.h"
#include <cstring>
#include <iostream>
using namespace std;

bool check_name_dogd(osadki* element)
{
	return strcmp(element->title, "�����") == 0;
}
bool check_kolvo(osadki* element)
{
	return element->kolvo < 15;
}
osadki** filter(osadki* array[], int size, bool (*check)(osadki* element), int& result_size)
{
	osadki** result = new osadki* [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}
